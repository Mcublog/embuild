#!/bin/bash

apt-get update
apt-get install -y libx11-6 libfreetype6 libxrender1 libfontconfig1 libxext6 xvfb git wget

Xvfb :1 -screen 0 1024x768x16 &

mkdir -p /_tmp
cd /_tmp
wget --no-check-certificate -qO SES https://www.segger.com/downloads/embedded-studio/Setup_EmbeddedStudio_ARM_v622a_linux_x64.tar.gz
tar xvf SES
printf 'yes\n' | DISPLAY=:1 $(find arm_segger_* -name "install_segger*") --copy-files-to /ses && \
cd /
rm -rf /_tmp

./ses/bin/pkg update
./ses/bin/pkg install -yes libcxx_arm libcxx_arm_v7em

apt-get remove -y wget
