FROM mcr.microsoft.com/powershell:latest

ENV SES_SCRIPT="ses_install.sh"

ADD ${SES_SCRIPT} /
RUN chmod +x /${SES_SCRIPT}
RUN /${SES_SCRIPT}

ENV PATH="/ses/bin:$PATH"

RUN ln -s /usr/bin/pwsh /usr/bin/powershell

CMD ["/bin/bash"]